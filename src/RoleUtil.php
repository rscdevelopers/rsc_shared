<?php

namespace Drupal\rsc_shared;

use Drupal\user\Entity\Role;
use Exception;

/**
 * Utilities for working with user roles and permissions.
 *
 * @package Drupal\rsc_shared
 */
class RoleUtil {

  /**
   * Grant multiple permissions to multiple roles.
   *
   * @param array $role_ids
   *   The IDs of the roles for which to grant the permissions.
   * @param array $permissions
   *   The permissions to grant to the given roles.
   *
   * @throws \Exception
   *   When a role does not exist.
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   When a role fails to save.
   */
  public static function grant(array $role_ids, array $permissions): void {
    foreach ($role_ids as $role_id) {
      /** @var \Drupal\user\RoleInterface $role */
      $role = Role::load($role_id);
      if (!$role) {
        throw new Exception("Role with ID '$role_id' does not exist.");
      }

      foreach ($permissions as $permission) {
        $role->grantPermission($permission);
      }
      $role->save();
    }
  }

  /**
   * Grant permissions to roles as specified in a nested array.
   *
   * @param array $permissions_by_role
   *   An array of arrays. The first level is keyed by the IDs of the roles that
   *   should receive the permissions. The second level lists the permissions to
   *   grant to that role.
   *
   * @throws \Exception
   *   When a role does not exist.
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   When a role fails to save.
   */
  public static function grantPermissionsByRole(array $permissions_by_role) {
    foreach ($permissions_by_role as $role_id => $permissions) {
      self::grant([$role_id], $permissions);
    }
  }

}
